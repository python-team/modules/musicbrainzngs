musicbrainzngs (0.7.1-6) unstable; urgency=medium

  * Team upload
  * debian/patches: Fix build with sphinx 7.1 (Closes: #1042619)

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 01 Nov 2023 18:50:17 +0100

musicbrainzngs (0.7.1-5) unstable; urgency=medium

  * Team upload
  * debian/: Remove use of python-libdiscid-doc (Closes: #1043279)

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 23 Aug 2023 09:43:23 +0200

musicbrainzngs (0.7.1-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on libc-bin and python3-sphinx.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 17 Oct 2022 01:19:44 +0100

musicbrainzngs (0.7.1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.
  * Apply multi-arch hints.
    + python-musicbrainzngs-doc: Add Multi-Arch: foreign.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Wed, 04 May 2022 00:29:05 -0400

musicbrainzngs (0.7.1-2) unstable; urgency=medium

  * Team upload.
  * Remove Recommends on non-existent Python2 package (Closes: #945669)

 -- Scott Talbert <swt@techie.net>  Fri, 08 May 2020 16:45:48 -0400

musicbrainzngs (0.7.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Josue Ortega ]
  * New upstream release (0.7.1)
    + Remove 0004-Disable-rate-limiting-in-test_requests.patch.
    + Update use_local_intersphinx_links patch
    + Remove 0002-Open-test-XML-as-binary.patch. Patch is no longer need,
      it has been applied by upstream
    + Remove 0003-Set-useragent-and-do-auth-in-test_submit-setup.patch.
      Patch is no longer need, it has been applied by upstream
  * debian/control:
    + Add Rules-Requires-Root field
    + Add python3-setuptools as build dependency

 -- Josue Ortega <josue@debian.org>  Tue, 14 Jan 2020 20:59:16 -0600

musicbrainzngs (0.6-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Remove ancient X-Python3-Version field.

  [ Josue Ortega ]
  * Drop python 2 package
  * Bump debhelper-compat to (= 12)
  * Add examples installation
  * Bump Standards-Version to 4.4.0. No changes required.
  * python-musicbrainzngs-doc.doc-base:
    - Fix typo at Document field
    - Fix HTML content location

 -- Josue Ortega <josue@debian.org>  Thu, 15 Aug 2019 13:56:20 -0600

musicbrainzngs (0.6-3) unstable; urgency=medium

  [Ondřej Nový]
  * d/control: Set Vcs-* to salsa.debian.org.
  * d/copyright: Use https protocol in Format field.

  [Josue Ortega]
  * Bumps compat level from 9 to 11.
  * debian/control:
   - Updates Uploaders field (Closes: #889774).
   - Bumps X-Python3-Version to >= 3.5.
   - Bumps Standards-Version from 3.9.8 to 4.1.3, no changes required.
   - Adds python3-sphinx dependency.
  * Updates python-musicbrainzngs-doc.doc-base.

 -- Josue Ortega <josue@debian.org>  Sun, 18 Feb 2018 19:15:05 -0600

musicbrainzngs (0.6-2) unstable; urgency=medium

  * Team upload.

  [ Stefano Rivera ]
  * Build-Depend on python-libdiscid-doc, so it can actually find the
    intersphinx metadata.
  * Add autopkgtests.

  [ Sebastian Ramacher ]
  * Add upstream patches to make tests independent of each other. (Closes:
    #842205)

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 20 Dec 2016 20:19:07 +0100

musicbrainzngs (0.6-1) unstable; urgency=medium

  [ Stefano Rivera ]
  * Team upload.
  * New upstream release.
  * Use link to debian's python-libdiscid-doc. (Closes: #830554)
  * Patch: Open test XML files in binary mode, so the tests pass under
    python3.
  * Use the debian changelog's date in docs, to build reproducibly. Thanks
    Juan Picca for the patch. (Closes: #788477)
  * Bump debhelper compat level to 9.
  * Bump Standards-Version to 3.9.8, no changes needed.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

 -- Stefano Rivera <stefanor@debian.org>  Sat, 20 Aug 2016 19:53:58 -0700

musicbrainzngs (0.5-2) unstable; urgency=medium

  * Port the build system to pybuild
  * Bump copyright years

 -- Simon Chopin <chopin.simon@gmail.com>  Mon, 13 Oct 2014 10:53:41 +0200

musicbrainzngs (0.5-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Run tests only if DEB_BUILD_OPTIONS=nocheck is not set.

  [ Simon Chopin ]
  * New upstream release (Closes: #747333)
    + Drop remove_unused_data and fix_undefined_name patches, integrated
      upstream
  * Update d/copyright to add musicbrainzngs/compat.py specifics.
  * Bump standards version to 3.9.6 (no change needed)

 -- Simon Chopin <chopin.simon@gmail.com>  Tue, 30 Sep 2014 19:24:50 +0200

musicbrainzngs (0.4-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Simon Chopin ]
  * Upload to unstable
  * New upstream release (Closes: #706961)
  * Acknowledge Jonas' NMU, drop the patch as it is now unnecessary
  * Bump standards version to 3.9.4
    + B-D on debhelper >= 8.1 to get the build-indep targets.
  * Update copyright years and authors.
  * Add a Python3 and a -doc packages.
    + Patch the sphinx config to use local documentation for reference
    + Remove the test data and docs from setup.py as they are not necessary
      in the binary package or shipped on their own.
  * Remove python-unittest2 from Build-Depends as python 2.6 isn't supported
    anymore.
  * Fix undefined name in the barcode handling.

 -- Simon Chopin <chopin.simon@gmail.com>  Sun, 26 May 2013 23:01:13 +0200

musicbrainzngs (0.2-1.1) experimental; urgency=low

  * Non-maintainer upload.
  * Add patch to fix get_release_by_discid(). Needed by morituri.
    Closes: bug#698202.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 23 Jan 2013 14:46:49 +0100

musicbrainzngs (0.2-1) unstable; urgency=low

  * New upstream release
    - patch unittest2.patch not needed any more.
  * Bump standards version to 3.9.3.

 -- Simon Chopin <chopin.simon@gmail.com>  Sun, 08 Apr 2012 21:24:49 +0100

musicbrainzngs (0.1-1) unstable; urgency=low

  * Initial release (Closes: #655973)
  * Add patch unittest2.patch to fix the tests with Python 2.6

 -- Simon Chopin <chopin.simon@gmail.com>  Wed, 18 Jan 2012 15:57:14 +0200
